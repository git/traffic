test:V:api/api.yml api/api.bare
	git clone https://git.sr.ht/~sircmpwn/go-bare
	cd go-bare
	go build -o gen cmd/gen/main.go
	./gen -p api ../api/api.bare structs_gen.go
	cd ../
	asciidoc traffic.adoc

lint:V:api/api.yml
	git clone https://github.com/daveshanley/vacuum.git
	cd vacuum
	go build vacuum.go
	./vacuum lint -dbq ../api/api.yml
	cd ../

clean:V:
	rm -rf vacuum go-bare traffic.html
